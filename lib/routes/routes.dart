import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/dashboard.dart';
import 'package:flutter_application_1/pages/first_screen.dart';
import 'package:flutter_application_1/pages/login.dart';
import 'package:flutter_application_1/pages/second_screen.dart';
import 'package:flutter_application_1/pages/eddit_profile.dart';

class ScreenManager {
  static const String login = '/';
  static const String dashboard = '/dashboad';
  static const String firtsScreen = '/firstscreen';
  static const String secondScreen = '/secondscreen';
  static const String edditProfile = '/edditProfile';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    
    switch (settings.name) {
      case login:
        return MaterialPageRoute(builder: (context) => Login());
      case dashboard:
        return MaterialPageRoute(builder: (context) => Dashboard());
      case firtsScreen:
        return MaterialPageRoute(builder: (context) => FirstScreen());
      case secondScreen:
        return MaterialPageRoute(builder: (context) => SecondScreen());
       case edditProfile:
        return MaterialPageRoute(builder: (context) => EdditProfile());
      default:
        throw FormatException("The selected screen is not valid ");
    }
  }
}
