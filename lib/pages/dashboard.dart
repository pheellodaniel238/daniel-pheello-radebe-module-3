import 'package:flutter/material.dart';
import 'package:flutter_application_1/routes/routes.dart';

class Dashboard extends StatefulWidget {
  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
      ),
      body: Center(
        child:Column(children: [ 
        ElevatedButton(
          onPressed:(() =>Navigator.of(context).pushNamed(ScreenManager.firtsScreen)//On cliked go to the next screen
          ),
        child: Text("First Screen"),),
       ElevatedButton(
          onPressed:(() =>Navigator.of(context).pushNamed(ScreenManager.secondScreen)//On cliked go to the next screen
          ),
        child: Text("Go to the next Screen"),),
        ElevatedButton(
          onPressed:(() =>Navigator.of(context).pushNamed(ScreenManager.edditProfile)//On cliked go to the next screen
          ),
        child: Text("Eddit Profile"),)
      ]))
    );
  }
}
