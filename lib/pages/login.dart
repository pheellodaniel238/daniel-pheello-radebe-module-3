import 'package:flutter/material.dart';
import 'package:flutter_application_1/routes/routes.dart';

class Login extends StatefulWidget {
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        decoration:  BoxDecoration(   gradient: LinearGradient(
                   begin: Alignment.topRight,
                   end:Alignment.bottomLeft ,
                  colors: [Colors.white70,Colors.blue,Colors.white])), 
          child: ListView(
        padding: const EdgeInsets.all(50),
        children: <Widget>[
          Text("Welcome",
              style: TextStyle(
                color: Colors.white70,
                fontSize: 40,
              )),
          SizedBox(height: 20),
          TextField(
            style: TextStyle(color: Colors.white70),
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Username",
                labelStyle: TextStyle(
                  color: Colors.white60,
                )),
          ),
          SizedBox(height: 20),
          TextField(
            style: TextStyle(color: Colors.white70),
            obscureText: true,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Password",
                labelStyle: TextStyle(
                  color: Colors.white60,
                )),
          ),
          SizedBox(height: 20),
          ElevatedButton(
            onPressed: (() =>
                Navigator.of(context).pushNamed(ScreenManager.dashboard)),
            child: Text("Sing in"),),
            Row(children: <Widget>[
          Text("Dont\'t have an account?"),
          TextButton(onPressed: (){},
          child: const Text("Register"))
          ])
        ],
      )),
    ));
  }
}
