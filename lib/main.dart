import 'package:flutter/material.dart';
import 'package:flutter_application_1/routes/routes.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "TechRides",
      initialRoute: ScreenManager.login,
      onGenerateRoute: ScreenManager.generateRoute,
      
    );
  }
}

